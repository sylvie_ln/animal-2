{
    "id": "2d3e2c21-9c4d-49d3-816a-27a9594efca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88486c04-660e-479f-8a42-b29ff7664a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3e2c21-9c4d-49d3-816a-27a9594efca3",
            "compositeImage": {
                "id": "83dfeae9-2e18-4113-827b-97b163785c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88486c04-660e-479f-8a42-b29ff7664a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bad08b-b4a6-42c7-994b-f56aaa7d756b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88486c04-660e-479f-8a42-b29ff7664a23",
                    "LayerId": "2318110e-446e-476f-b752-042102b4b89d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2318110e-446e-476f-b752-042102b4b89d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d3e2c21-9c4d-49d3-816a-27a9594efca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}