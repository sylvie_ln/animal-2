{
    "id": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2a0dde4-2c4f-48cf-bfdb-11462b2276c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
            "compositeImage": {
                "id": "25b7ed47-fd3d-4bc4-ac6c-960460755e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a0dde4-2c4f-48cf-bfdb-11462b2276c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9414b4a0-57d6-43f9-9e07-e84ea0fc94c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a0dde4-2c4f-48cf-bfdb-11462b2276c3",
                    "LayerId": "5b1fbdbe-ab67-4b5b-9a07-22a0687dbaa3"
                }
            ]
        },
        {
            "id": "468b6db9-dc03-424f-a3e4-be5e106b8779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
            "compositeImage": {
                "id": "1065109a-5a8f-48df-a839-2a9f7eed7938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468b6db9-dc03-424f-a3e4-be5e106b8779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b45958-de78-4128-bc68-a75aaec23436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468b6db9-dc03-424f-a3e4-be5e106b8779",
                    "LayerId": "5b1fbdbe-ab67-4b5b-9a07-22a0687dbaa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5b1fbdbe-ab67-4b5b-9a07-22a0687dbaa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}