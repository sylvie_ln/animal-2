{
    "id": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharFlip2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "380136d9-3490-4d8c-a4dc-fa5475cc89b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
            "compositeImage": {
                "id": "a9d65f5a-a6ff-455b-8456-1aa9bd19f823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380136d9-3490-4d8c-a4dc-fa5475cc89b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf23282d-0a8c-4819-9275-523c8c3dc58c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380136d9-3490-4d8c-a4dc-fa5475cc89b2",
                    "LayerId": "48d3781e-770e-473f-b900-cbd784a3486f"
                }
            ]
        },
        {
            "id": "8f5739e6-662a-4c36-abc3-3350bd6843bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
            "compositeImage": {
                "id": "78f2000c-61cc-4e85-be5c-5aeb70618298",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5739e6-662a-4c36-abc3-3350bd6843bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787dcbf9-5706-4744-b46e-83f2ec295aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5739e6-662a-4c36-abc3-3350bd6843bf",
                    "LayerId": "48d3781e-770e-473f-b900-cbd784a3486f"
                }
            ]
        },
        {
            "id": "7230bdaf-0fc6-46ac-aaa5-9460a860bd0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
            "compositeImage": {
                "id": "6aaff2f5-b28b-45a5-8e9d-bdc476e4775e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7230bdaf-0fc6-46ac-aaa5-9460a860bd0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f7b673-9cac-4df6-878e-61fbe2060391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7230bdaf-0fc6-46ac-aaa5-9460a860bd0e",
                    "LayerId": "48d3781e-770e-473f-b900-cbd784a3486f"
                }
            ]
        },
        {
            "id": "30468562-8636-4703-b457-7046870cc2ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
            "compositeImage": {
                "id": "57e28217-d37a-4072-8d99-3bae21f4b745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30468562-8636-4703-b457-7046870cc2ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7089c89-9d2d-47bd-9eb4-c2fba75d4725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30468562-8636-4703-b457-7046870cc2ea",
                    "LayerId": "48d3781e-770e-473f-b900-cbd784a3486f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "48d3781e-770e-473f-b900-cbd784a3486f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ed00eb6-2246-4f23-9a68-d608eca21feb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}