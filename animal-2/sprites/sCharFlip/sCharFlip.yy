{
    "id": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharFlip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b10cc5b-fcf3-4d49-968e-154f3d9f04a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
            "compositeImage": {
                "id": "1b8aab9a-8a87-43af-ad8c-37ae86a8f5c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b10cc5b-fcf3-4d49-968e-154f3d9f04a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664205af-5ebb-4984-9c27-674279c046e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b10cc5b-fcf3-4d49-968e-154f3d9f04a4",
                    "LayerId": "12db6207-5062-47fb-9d95-e20bb1944423"
                }
            ]
        },
        {
            "id": "9290409f-4c9c-442c-8362-e61607ac7f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
            "compositeImage": {
                "id": "cec98bad-d1bb-4207-9d4b-874c2a95e835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9290409f-4c9c-442c-8362-e61607ac7f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20547934-4eaf-4cc8-817b-db73d66d33cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9290409f-4c9c-442c-8362-e61607ac7f46",
                    "LayerId": "12db6207-5062-47fb-9d95-e20bb1944423"
                }
            ]
        },
        {
            "id": "cf433aad-24dc-4d9e-aabf-98bdb8aa90b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
            "compositeImage": {
                "id": "fad1cb9c-187d-4fac-9a2d-13d12ef81cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf433aad-24dc-4d9e-aabf-98bdb8aa90b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9820ddb-4a02-4a02-bffb-f0ab6d274afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf433aad-24dc-4d9e-aabf-98bdb8aa90b3",
                    "LayerId": "12db6207-5062-47fb-9d95-e20bb1944423"
                }
            ]
        },
        {
            "id": "fedac555-bb9f-45da-ab37-995a010432cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
            "compositeImage": {
                "id": "ebcbb099-1f77-42a1-9a3b-517b87f154cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedac555-bb9f-45da-ab37-995a010432cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f82c3c-313b-47ef-a8d4-3ad0f4123628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedac555-bb9f-45da-ab37-995a010432cb",
                    "LayerId": "12db6207-5062-47fb-9d95-e20bb1944423"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "12db6207-5062-47fb-9d95-e20bb1944423",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76e718cf-997b-4a3d-b0c7-9987245d50b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}