{
    "id": "cca5a1e7-2672-49d3-aa93-90838631660e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigDPad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e773a54b-47b8-4118-b71e-aeac405e92e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "compositeImage": {
                "id": "c87839f4-9cde-45de-b8c0-4ad8ba50de1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e773a54b-47b8-4118-b71e-aeac405e92e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c1594d7-0069-41ac-91fd-6cd63e93f230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e773a54b-47b8-4118-b71e-aeac405e92e9",
                    "LayerId": "9b30d54f-0e27-4a44-a793-3c60c69af132"
                },
                {
                    "id": "b7b08fbd-39c9-44e1-aee8-5d76e5626afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e773a54b-47b8-4118-b71e-aeac405e92e9",
                    "LayerId": "6a352bda-6f21-4ffc-bf44-2ed67622d968"
                }
            ]
        },
        {
            "id": "05cc7c1c-99b6-416d-b805-6709ee5adfb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "compositeImage": {
                "id": "3cebd135-855f-4621-a2a5-950ce499e2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05cc7c1c-99b6-416d-b805-6709ee5adfb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e50c27d9-0959-4f9c-ace2-bdecb1e83147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05cc7c1c-99b6-416d-b805-6709ee5adfb2",
                    "LayerId": "9b30d54f-0e27-4a44-a793-3c60c69af132"
                },
                {
                    "id": "6548c8d4-bdc1-4d9a-b168-c76026258afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05cc7c1c-99b6-416d-b805-6709ee5adfb2",
                    "LayerId": "6a352bda-6f21-4ffc-bf44-2ed67622d968"
                }
            ]
        },
        {
            "id": "3966c12d-2204-494f-9dec-573cf584626b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "compositeImage": {
                "id": "ebe18880-354e-4b43-9f13-ba879c32dc5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3966c12d-2204-494f-9dec-573cf584626b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "608e0711-d4d2-476a-a166-dba8a621bd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3966c12d-2204-494f-9dec-573cf584626b",
                    "LayerId": "9b30d54f-0e27-4a44-a793-3c60c69af132"
                },
                {
                    "id": "88225d5f-2760-4a35-b176-1582e724cba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3966c12d-2204-494f-9dec-573cf584626b",
                    "LayerId": "6a352bda-6f21-4ffc-bf44-2ed67622d968"
                }
            ]
        },
        {
            "id": "3be4fc99-2531-498d-a08e-6b8d3c0a82e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "compositeImage": {
                "id": "7ab3cde3-4a15-4160-9534-38f24b357d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be4fc99-2531-498d-a08e-6b8d3c0a82e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f5589b-c0dc-42db-9c25-e4e6e1c44949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be4fc99-2531-498d-a08e-6b8d3c0a82e4",
                    "LayerId": "9b30d54f-0e27-4a44-a793-3c60c69af132"
                },
                {
                    "id": "1ab832bb-8054-4bee-9438-de91eba7d769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be4fc99-2531-498d-a08e-6b8d3c0a82e4",
                    "LayerId": "6a352bda-6f21-4ffc-bf44-2ed67622d968"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 14,
    "layers": [
        {
            "id": "9b30d54f-0e27-4a44-a793-3c60c69af132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6a352bda-6f21-4ffc-bf44-2ed67622d968",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cca5a1e7-2672-49d3-aa93-90838631660e",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}