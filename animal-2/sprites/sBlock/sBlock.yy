{
    "id": "fd0dabb1-102a-42ed-91d7-f763c32346df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96a7b621-1648-40fb-ab48-35382ae35d9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0dabb1-102a-42ed-91d7-f763c32346df",
            "compositeImage": {
                "id": "b7b954bf-9432-4f93-9481-293fe5b091b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96a7b621-1648-40fb-ab48-35382ae35d9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119e017a-bdbf-489c-bdee-905ed80bafe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96a7b621-1648-40fb-ab48-35382ae35d9e",
                    "LayerId": "cca92d68-6175-4510-867b-1aed92d4ee6c"
                }
            ]
        },
        {
            "id": "7016f940-ba39-4cd0-a636-edfd9d147694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0dabb1-102a-42ed-91d7-f763c32346df",
            "compositeImage": {
                "id": "06b7a928-021b-4671-b72f-32ed0b22027a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7016f940-ba39-4cd0-a636-edfd9d147694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8984135-d8d2-4e9e-a672-185f184fd408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7016f940-ba39-4cd0-a636-edfd9d147694",
                    "LayerId": "cca92d68-6175-4510-867b-1aed92d4ee6c"
                }
            ]
        },
        {
            "id": "b812911b-ff6d-43b4-9400-f4479342794b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0dabb1-102a-42ed-91d7-f763c32346df",
            "compositeImage": {
                "id": "0fd0b145-7d87-4c6a-83a1-f1acd17ae031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b812911b-ff6d-43b4-9400-f4479342794b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd24e0a8-61c4-422f-86c7-451fd83ba7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b812911b-ff6d-43b4-9400-f4479342794b",
                    "LayerId": "cca92d68-6175-4510-867b-1aed92d4ee6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cca92d68-6175-4510-867b-1aed92d4ee6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd0dabb1-102a-42ed-91d7-f763c32346df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}