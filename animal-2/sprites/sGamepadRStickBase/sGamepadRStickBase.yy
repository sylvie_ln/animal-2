{
    "id": "9304f4a9-867c-4a4a-812a-80631c69ad24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepadRStickBase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 56,
    "bbox_right": 69,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65ac76eb-1762-4dba-aa15-0a33301e49ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9304f4a9-867c-4a4a-812a-80631c69ad24",
            "compositeImage": {
                "id": "d2621971-cd29-4cdc-a10e-a4452ac3313e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ac76eb-1762-4dba-aa15-0a33301e49ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f999d2-61b3-421b-9e86-a41282d3048f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ac76eb-1762-4dba-aa15-0a33301e49ba",
                    "LayerId": "53945696-4a63-4419-809a-2ba81a253a12"
                },
                {
                    "id": "67882b9c-3550-4593-a88b-7cec7b5846ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ac76eb-1762-4dba-aa15-0a33301e49ba",
                    "LayerId": "f751f850-d327-4f36-b387-58e830755a3a"
                }
            ]
        },
        {
            "id": "69513cf0-bb80-4adc-86df-f26d8ab82318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9304f4a9-867c-4a4a-812a-80631c69ad24",
            "compositeImage": {
                "id": "ed486d09-1168-41ef-b289-75adc4c28a8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69513cf0-bb80-4adc-86df-f26d8ab82318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa4150f-6ba3-436d-b014-730d36bf5057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69513cf0-bb80-4adc-86df-f26d8ab82318",
                    "LayerId": "53945696-4a63-4419-809a-2ba81a253a12"
                },
                {
                    "id": "345404b9-1aae-470e-9722-3f86785a2d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69513cf0-bb80-4adc-86df-f26d8ab82318",
                    "LayerId": "f751f850-d327-4f36-b387-58e830755a3a"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "53945696-4a63-4419-809a-2ba81a253a12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9304f4a9-867c-4a4a-812a-80631c69ad24",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f751f850-d327-4f36-b387-58e830755a3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9304f4a9-867c-4a4a-812a-80631c69ad24",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}