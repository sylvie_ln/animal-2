{
    "id": "c6d0573f-d0db-4e03-bacd-7b38cdadaa83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 151,
    "bbox_right": 313,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e693162c-ac31-4e2b-9a8f-aaad7cc8ac8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6d0573f-d0db-4e03-bacd-7b38cdadaa83",
            "compositeImage": {
                "id": "46cab50b-0037-405c-9ec7-9d21dfd1f126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e693162c-ac31-4e2b-9a8f-aaad7cc8ac8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9436757-ffa3-4006-8113-58ca5eec67f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e693162c-ac31-4e2b-9a8f-aaad7cc8ac8b",
                    "LayerId": "2f9335f1-537c-4ab6-ab23-10e31ddcff82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "2f9335f1-537c-4ab6-ab23-10e31ddcff82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6d0573f-d0db-4e03-bacd-7b38cdadaa83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}