var xp = argument[0];
var yp = argument[1];
ds_list_clear(actors);
var num = instance_place_list(xp,yp,oActor,actors,false);
for(var i=0; i<num; i++) {
	var actor = actors[|i];
	if actor.blocker { return true; }
}
return false;