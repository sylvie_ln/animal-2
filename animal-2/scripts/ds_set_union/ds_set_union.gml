///@param sets...
var base = ds_set_create();
for(var i=0; i<argument_count; i++) {
	var set = argument[i];
	var items = set[|0];
	for(var j=0; j<ds_list_size(items); j++) {
		ds_set_add(base,items[|j]);		
	}
}
return base;
