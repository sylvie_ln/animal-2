///@param sets...
var base = ds_set_create();
var first_set = argument[0];
var first_items = first_set[|0];
for(var i=0; i<ds_list_size(first_items); i++) {
	var item = first_items[|i];
	var in_every_set = true;
	show_debug_message("Item "+string(item));
	for(var j=1; j<argument_count; j++) {
		var set = argument[j];
		if !ds_set_exists(set,item) {
			show_debug_message("Item "+string(item)+" not in intersection");
			in_every_set = false;
			break;
		}
	}
	if in_every_set {
		show_debug_message("Item "+string(item)+" in intersection");
		ds_set_add(base,item);	
	}
}
return base;
