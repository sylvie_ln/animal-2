{
    "id": "f5a665f8-d55a-46f8-b011-454ad80a8e8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlock",
    "eventList": [
        {
            "id": "439efc4b-8757-401e-a029-4982b32689a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5a665f8-d55a-46f8-b011-454ad80a8e8f"
        },
        {
            "id": "46ec59c6-241e-474f-abd4-1b6beebe0b2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f5a665f8-d55a-46f8-b011-454ad80a8e8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "fd0dabb1-102a-42ed-91d7-f763c32346df",
    "visible": true
}