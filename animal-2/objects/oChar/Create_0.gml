event_inherited();

grav = 0.1;
hv = 0;
vv = 0;
spd = 2.5;
acc = spd/16;
fric = acc;
spd += fric;
acc += fric;
jmp = sqrt(2*grav*32);

walkoff_time = room_speed div 10;
walkoff_timer = walkoff_time;
jumped = false;

flip_buffer = 0;
flip_timer = 0;
flip_speed = [0,0];

run = false;


hunger = "You are hungry."
status = "Your status is fluffy."
jumper = "Jumping power: Medium"
