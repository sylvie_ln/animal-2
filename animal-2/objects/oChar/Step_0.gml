var left = input_held("Left");
var right = input_held("Right");
var hdir = right-left;
if left and right {
	hdir = input_held_time("Left") < input_held_time("Right") ? -1 : 1;	
}

if abs(hv+hdir*acc) <= spd or hdir != sign(hv) {
	hv += hdir*acc;	
} else {
	if hv > 0 and hdir > 0 {
		hv = max(hv,spd);	
	} else if hv < 0 and hdir < 0 {
		hv = min(hv,-spd);
	}
}

if hv != 0 {
	var s = sign(hv);
	hv -= fric*s;
	if sign(hv) != s {
		hv = 0;	
	}
}


var ong = collision_at(x,y+1);
if !ong {
	if walkoff_timer > 0 {
		walkoff_timer--;
	}
} else {
	if jumped {
		walkoff_timer = walkoff_time;	
		jumped = false;
	}
}
var float = walkoff_timer > 0;

if ong and vv = 0 {
	if image_yscale < 0 {
		image_yscale = 1;	
		y -= 3;
	}
}

/*
if input_pressed(oInput.act2) {
	image_yscale = -image_yscale;
	y -= image_yscale*3;
}
*/

if !float {
	vv += grav;	
}

if float and input_pressed(oInput.act1) and image_yscale > 0 {
	jumped = true;
	vv = -jmp;	
}

if !ong and input_released(oInput.act1) and image_yscale > 0 {
	flip_timer = flip_buffer;
	flip_speed = [hv,vv*sqrt(2)];
}

if flip_timer > -1 {
	flip_timer--;
	if flip_timer == -1 {
		if !ong {
			if image_yscale > 0 {
				vv = flip_speed[1];
				image_yscale = -1;
				y += 3;
			}	
		}
		flip_speed = [0,0];
	}
}


if !move(hv,0) {
	hv = -hv*sqrt(2);
	if abs(hv) < spd {
		if hv == 0 {
			hv = spd*(-hdir);
		} else {
			hv = spd*sign(hv);	
		}
	}
	hv = clamp(hv,-2*spd,2*spd);
}
if !move(vv,1) {
	if vv > 0 {
		if image_yscale < 0 {
			vv = -vv*0.6;
			if abs(vv) < 0.5 {
				vv = 0;
				walkoff_timer = walkoff_time;	
			}
		} else {
			vv = 0;	
			walkoff_timer = walkoff_time;	
		}
	} else {
		if image_yscale > 0 {
			image_yscale = -1;
			y += 3;
		}
		vv = -vv;	
	}
}

if hdir != 0 {
	image_xscale = hdir;	
}

run = false;
if image_yscale < 0 {
	image_speed = 1;	
} else {
	if float {
		if hdir != 0 {
			image_speed = 1;	
			run = true;
		} else {
			image_speed = 0;
			image_index = 0;
		}
	} else {
		image_speed = 0;
		image_index = 1;
	}
}
