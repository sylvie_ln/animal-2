{
    "id": "fe321dcd-b93b-4ddd-8004-3530ed31003a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChar",
    "eventList": [
        {
            "id": "0e0efea1-ad74-4d6d-b058-290944f74e56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe321dcd-b93b-4ddd-8004-3530ed31003a"
        },
        {
            "id": "1db3dea3-306b-4cc2-af62-c94de0b683b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe321dcd-b93b-4ddd-8004-3530ed31003a"
        },
        {
            "id": "0ec2fa85-524a-4a7a-8225-694fd66c5291",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fe321dcd-b93b-4ddd-8004-3530ed31003a"
        }
    ],
    "maskSpriteId": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c1a91f22-e100-4cc9-a508-80cb37b045d6",
    "visible": true
}